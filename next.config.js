/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  exportTrailingSlash: true, // Ajoute un slash final à chaque URL
}

module.exports = nextConfig
