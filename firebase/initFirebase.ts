import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import { getPerformance, FirebasePerformance } from 'firebase/performance';

const firebaseConfig = {
  apiKey: "AIzaSyBzS3pmZs0WMzNA40LBHuUG8GZRx0Co8ig",
  authDomain: "mspr-bloc-4-5135a.firebaseapp.com",
  projectId: "mspr-bloc-4-5135a",
  storageBucket: "mspr-bloc-4-5135a.appspot.com",
  messagingSenderId: "405778108809",
  appId: "1:405778108809:web:2f6542855e3f78b6d51158",
  measurementId: "G-G1S80GHD76"
};

// Initialisation de l'application Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);

// Initialisation du service Firebase Performance uniquement côté client
let performance: FirebasePerformance | undefined;

if (typeof window !== "undefined") {  // Vérifier que l'environnement est côté client
  try {
    performance = getPerformance(app);
  } catch (error) {
    console.error("Le service Firebase Performance n'est pas disponible :", error);
  }
}

export { db, auth, performance };