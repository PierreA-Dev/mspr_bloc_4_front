import 'whatwg-fetch';
import '@testing-library/jest-dom'; // Pour des assertions supplémentaires sur le DOM
import fetchMock from 'jest-fetch-mock';

fetchMock.enableMocks();
