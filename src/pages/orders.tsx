import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useAuth } from '../context/AuthContext';
import { useRouter } from 'next/router';

interface Commande {
  email: string;
  numcommandes: string;
  refproduits: string;
  libelle: string;
  quantite: number;
  poids: number;
  date_expedition: string;
  conditionnement: string;
  etat: string;
}
 
const OrdersPage: React.FC = () => {
  const { user } = useAuth();
  const [commandes, setCommandes] = useState<Commande[]>([]);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  useEffect(() => {
    if (!user) {
      router.push('/login');
    } else {
      const fetchCommandes = async () => {
        try {
          const token = await user.getIdToken(); // Récupération du token d'authentification
          const response = await axios.get(`https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes/${user.email}`, {
            headers: {
              Authorization: `Bearer ${token}`,
              'Cache-Control': 'no-cache',
              'Pragma': 'no-cache',
              'Expires': '0',
            },
          });
          console.log('Commandes Response:', response.data); // Log pour vérifier les données récupérées
          setCommandes(response.data);
        } catch (error) {
          console.error('Erreur lors de la récupération des commandes:', error);
        } finally {
          setLoading(false);
        }
      };

      fetchCommandes();
    }
  }, [user, router]);

  if (loading) {
    return <div>Chargement...</div>;
  }

  if (!user) {
    return <div>Veuillez vous connecter pour voir cette page.</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Mes Commandes</h1>
      {commandes.length === 0 ? (
        <div>Aucune commande trouvée.</div>
      ) : (
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th>Email</th>
                <th>Numéro</th>
                <th>Référence Produit</th>
                <th>Libellé</th>
                <th>Quantité</th>
                <th>Poids</th>
                <th>Date Expédition</th>
                <th>Conditionnement</th>
                <th>État</th>
              </tr>
            </thead>
            <tbody>
              {commandes.map((commande, index) => (
                <tr key={index}>
                  <td>{commande.email}</td>
                  <td>{commande.numcommandes}</td>
                  <td>{commande.refproduits}</td>
                  <td>{commande.libelle}</td>
                  <td>{commande.quantite}</td>
                  <td>{commande.poids}</td>
                  <td>{commande.date_expedition}</td>
                  <td>{commande.conditionnement}</td>
                  <td>{commande.etat}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default OrdersPage;