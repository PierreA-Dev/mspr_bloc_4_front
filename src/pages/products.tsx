import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Image from 'next/image';
import { useCart } from '../context/CartContext';
import { trace } from 'firebase/performance';
import { performance } from '../../firebase/initFirebase';

interface Product {
  id: string;
  reference: string;
  libelle: string;
  format: string;
  pays: string;
  intensite: string;
  aromes: string;
  conditionnement: string;
  image: string;
  prix: string;
}

const ProductsPage: React.FC = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [loading, setLoading] = useState(true);
  const { dispatch } = useCart();

  useEffect(() => {
    const fetchProducts = async () => {
      if (typeof window !== 'undefined' && performance) {
        const apiTrace = trace(performance, "fetchProducts");
        apiTrace.start();
        try {
          const response = await axios.get('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits');
          setProducts(response.data);
        } catch (error) {
          console.error('Erreur lors de la récupération des produits:', error);
        } finally {
          apiTrace.stop();
          setLoading(false);
        }
      } else {
        try {
          const response = await axios.get('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits');
          setProducts(response.data);
        } catch (error) {
          console.error('Erreur lors de la récupération des produits:', error);
        } finally {
          setLoading(false);
        }
      }
    };

    fetchProducts();
  }, []);

  const addToCart = (product: Product) => {
    console.log('Adding to cart:', product); // Log pour vérifier que la fonction est appelée
    dispatch({ type: 'ADD_TO_CART', product });
  };

  if (loading) {
    return <div>Chargement...</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-bold mb-8 text-center">Nos produits</h1>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
        {products.map(product => (
          <div key={product.id} className="card bg-white shadow-md rounded-lg overflow-hidden">
            <figure className="w-full h-64 relative">
              <Image src={`/image/${product.image}`} alt={product.libelle} layout="fill" className="object-cover" />
            </figure>
            <div className="card-body p-4">
              <h2 className="card-title text-xl font-semibold mb-2">{product.libelle}</h2>
              <p className="text-gray-700 mb-1">Prix: {product.prix} €</p>
              <p className="text-gray-700 mb-4">Pays: {product.pays}</p>
              <button 
                className="bg-black text-white py-2 px-4 text-lg rounded transition-transform transform hover:scale-105"
                onClick={() => addToCart(product)}>
                Ajouter au panier
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductsPage;