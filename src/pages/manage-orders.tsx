import React, { useEffect, useState, useCallback } from 'react';
import axios from 'axios';
import { useAuth } from '../context/AuthContext';



interface Commande {
  email: string;
  numcommandes: string;
  refproduits: string;
  libelle: string;
  quantite: number;
  conditionnement: string;
  etat: string;
}

const ManageOrdersPage: React.FC = () => {
  const [orders, setOrders] = useState<Commande[]>([]);
  const [editingOrder, setEditingOrder] = useState<Commande | null>(null);
  const { user } = useAuth();

  const fetchOrders = useCallback(async () => {
    if (!user) return;
    try {
      const token = await user.getIdToken(); // Récupération du token d'authentification
      const response = await axios.get('https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes', {
        headers: { Authorization: `Bearer ${token}` },
      });
      setOrders(response.data);
    } catch (error) {
      console.error('Erreur lors de la récupération des commandes:', error);
    }
  }, [user]);

  useEffect(() => {
    fetchOrders();
  }, [fetchOrders]);

  const handleUpdate = async () => {
    if (!editingOrder) return;

    try {
      const token = await user.getIdToken(); // Récupération du token d'authentification
      await axios.put(
        `https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes/${editingOrder.email}/${editingOrder.numcommandes}/${editingOrder.refproduits}`,
        editingOrder,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      setEditingOrder(null);
      fetchOrders(); // Rafraîchir les commandes après la mise à jour
    } catch (error) {
      console.error('Erreur lors de la mise à jour de la commande:', error);
    }
  };

  const handleDelete = async (order: Commande) => {
    try {
      const token = await user.getIdToken(); // Récupération du token d'authentification
      await axios.delete(
        `https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes/${order.email}/${order.numcommandes}/${order.refproduits}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      fetchOrders(); // Rafraîchir les commandes après la suppression
    } catch (error) {
      console.error('Erreur lors de la suppression de la commande:', error);
    }
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Gestion des commandes</h1>
      {orders.map((order, index) => (
        <div key={index} className="mb-4 p-4 border rounded-lg">
          <p><strong>Email:</strong> {order.email}</p>
          <p><strong>Num Commande:</strong> {order.numcommandes}</p>
          <p><strong>Produit:</strong> {order.libelle}</p>
          <p><strong>Quantité:</strong> {order.quantite}</p>
          <p><strong>Conditionnement:</strong> {order.conditionnement}</p>
          <p><strong>État:</strong> {order.etat}</p>
          <button className="btn btn-secondary mr-2" onClick={() => setEditingOrder(order)}>Modifier</button>
          <button className="btn btn-error" onClick={() => handleDelete(order)}>Supprimer</button>
        </div>
      ))}
      {editingOrder && (
        <div className="mt-4">
          <h2 className="text-xl font-bold mb-2">Modifier la commande</h2>
          <input
            type="text"
            value={editingOrder.libelle}
            onChange={(e) => setEditingOrder({ ...editingOrder, libelle: e.target.value })}
            className="input input-bordered mb-2"
          />
          <input
            type="number"
            value={editingOrder.quantite}
            onChange={(e) => setEditingOrder({ ...editingOrder, quantite: parseInt(e.target.value) })}
            className="input input-bordered mb-2"
          />
          <input
            type="text"
            value={editingOrder.conditionnement}
            onChange={(e) => setEditingOrder({ ...editingOrder, conditionnement: e.target.value })}
            className="input input-bordered mb-2"
          />
          <input
            type="text"
            value={editingOrder.etat}
            onChange={(e) => setEditingOrder({ ...editingOrder, etat: e.target.value })}
            className="input input-bordered mb-2"
          />
          <button className="btn btn-primary" onClick={handleUpdate}>Sauvegarder</button>
          <button className="btn btn-secondary ml-2" onClick={() => setEditingOrder(null)}>Annuler</button>
        </div>
      )}
    </div>
  );
};

export default ManageOrdersPage;