import { useState, useEffect } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useAuth } from '../context/AuthContext';
import React from 'react';
import { useCallback } from 'react';

interface Produit {
  id?: string;
  reference: string;
  libelle: string;
  format: string;
  pays: string;
  intensite: string;
  aromes: string;
  conditionnement: string;
  prix: string;
}

const ManageProducts = () => {
  const [produits, setProduits] = useState<Produit[]>([]);
  const [form, setForm] = useState<Produit>({
    reference: '',
    libelle: '',
    format: '',
    pays: '',
    intensite: '',
    aromes: '',
    conditionnement: '',
    prix: '',
  });
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const router = useRouter();
  const { user } = useAuth(); // Utilisez le contexte d'authentification

  const fetchProduits = useCallback(async () => {
    try {
        const token = await user.getIdToken(); // Récupération du token d'authentification
        const response = await axios.get('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits', {
            headers: { Authorization: `Bearer ${token}` },
        });
        setProduits(response.data);
    } catch (error) {
        console.error('Erreur:', error);
    }
}, [user]); // Ajoutez 'user' comme dépendance


useEffect(() => {
  if (user) {
      fetchProduits();
  }
}, [user, fetchProduits]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const token = await user.getIdToken(); // Récupération du token d'authentification
      if (isEditing) {
        await axios.put(`https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits/${form.id}`, form, {
          headers: { Authorization: `Bearer ${token}` },
        });
      } else {
        await axios.post('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits', form, {
          headers: { Authorization: `Bearer ${token}` },
        });
      }
      setForm({
        reference: '',
        libelle: '',
        format: '',
        pays: '',
        intensite: '',
        aromes: '',
        conditionnement: '',
        prix: '',
      });
      setIsEditing(false);
      fetchProduits();
    } catch (error) {
      console.error('Erreur:', error);
    }
  };

  const handleEdit = (produit: Produit) => {
    setForm(produit);
    setIsEditing(true);
  };

  const handleDelete = async (id: string) => {
    try {
      const token = await user.getIdToken(); // Récupération du token d'authentification
      await axios.delete(`https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
      fetchProduits();
    } catch (error) {
      console.error('Erreur:', error);
    }
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Gestion des Produits</h1>
      <form className="mb-4" onSubmit={handleSubmit}>
        <div className="grid grid-cols-2 gap-4">
          <input
            className="input input-bordered"
            type="text"
            name="reference"
            placeholder="Référence"
            value={form.reference}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="libelle"
            placeholder="Libellé"
            value={form.libelle}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="format"
            placeholder="Format"
            value={form.format}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="pays"
            placeholder="Pays"
            value={form.pays}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="intensite"
            placeholder="Intensité"
            value={form.intensite}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="aromes"
            placeholder="Arômes"
            value={form.aromes}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="conditionnement"
            placeholder="Conditionnement"
            value={form.conditionnement}
            onChange={handleInputChange}
            required
          />
          <input
            className="input input-bordered"
            type="text"
            name="prix"
            placeholder="Prix"
            value={form.prix}
            onChange={handleInputChange}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary mt-4">
          {isEditing ? 'Modifier' : 'Créer'}
        </button>
      </form>

      <h2 className="text-xl font-semibold mb-2">Liste des Produits</h2>
      <ul className="list-disc list-inside">
        {produits.map((produit) => (
          <li key={produit.id} className="mb-2">
            {produit.libelle} - {produit.prix}€
            <button
              className="btn btn-secondary ml-2"
              onClick={() => handleEdit(produit)}
            >
              Modifier
            </button>
            <button
              className="btn btn-error ml-2"
              onClick={() => handleDelete(produit.id!)}
            >
              Supprimer
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ManageProducts;