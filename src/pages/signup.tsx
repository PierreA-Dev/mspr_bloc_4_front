import React from 'react';
import AuthForm from '../components/AuthForm'; // Importer le composant AuthForm

const SignupPage: React.FC = () => {
  return (
    <AuthForm isSignup={true} />
  );
};

export default SignupPage;
