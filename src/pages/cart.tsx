import React from 'react';
import { useCart } from '../context/CartContext';
import { useAuth } from '../context/AuthContext';
import { useRouter } from 'next/router';
import axios from 'axios';
import { generateId } from '../utils/generateId';
import { performance } from '../../firebase/initFirebase';
import { trace } from 'firebase/performance';

const CartPage: React.FC = () => {
  const { state, dispatch } = useCart();
  const { user } = useAuth();
  const router = useRouter();

  console.log('Cart state in cart page:', state);

  const handleOrder = async () => {
    if (!user) {
      router.push('/login');
      return;
    }

    const numcommandes = generateId();

    try {
      const commandes = state.products.map(product => ({
        email: user.email,
        numcommandes,
        refproduits: product.reference,
        libelle: product.libelle,
        quantite: 1,
        conditionnement: product.conditionnement,
        etat: 'En cours',
      }));

      if (typeof window !== 'undefined' && performance) {
        const apiTrace = trace(performance, "createOrder");
        apiTrace.start();

        for (const commande of commandes) {
          await axios.post('https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes', commande, {
            headers: { Authorization: `Bearer ${await user.getIdToken()}` }
          });
        }

        apiTrace.stop();
      } else {
        for (const commande of commandes) {
          await axios.post('https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes', commande, {
            headers: { Authorization: `Bearer ${await user.getIdToken()}` }
          });
        }
      }

      alert('Commande créée avec succès!');
      dispatch({ type: 'CLEAR_CART' });
      router.push('/orders');
    } catch (error) {
      console.error('Erreur lors de la création de la commande:', error);
      alert('Erreur lors de la création de la commande.');
    }
  };

  if (state.products.length === 0) {
    return <div className="container mx-auto p-4">Votre panier est vide.</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Panier</h1>
      <div className="overflow-x-auto">
        <table className="table w-full">
          <thead>
            <tr>
              <th>Produit</th>
              <th>Référence</th>
              <th>Prix</th>
            </tr>
          </thead>
          <tbody>
            {state.products.map((product, index) => (
              <tr key={index}>
                <td>{product.libelle}</td>
                <td>{product.reference}</td>
                <td>{product.prix}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div>
        <button className="btn btn-primary" onClick={handleOrder}>
          Commander
        </button>
      </div>
    </div>
  );
};

export default CartPage;