import { getDocs, collection, addDoc } from 'firebase/firestore';
import { db } from '../../../../firebase/initFirebase';

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader && authHeader.startsWith('Bearer ')) {
    const token = authHeader.split(' ')[1];
    // Vous pouvez ajouter ici la logique de vérification du token
    next();
  } else {
    res.status(401).json({ error: 'Unauthorized' });
  }
};

export default async function handler(req, res) {
  if (req.method === 'GET') {
    // Enlever la protection pour la route GET
    try {
      const querySnapshot = await getDocs(collection(db, 'produits'));
      const documents = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
      res.status(200).json(documents);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  } else if (req.method === 'POST') {
    authenticateToken(req, res, async () => {
      try {
        const { reference, libelle, format, pays, intensite, aromes, conditionnement, prix } = req.body;
        if (!reference || !libelle || !format || !pays || !intensite || !aromes || !conditionnement || !prix) {
          return res.status(400).json({ error: 'Tous les champs sont obligatoires.' });
        }
        const docRef = await addDoc(collection(db, 'produits'), {
          reference, libelle, format, pays, intensite, aromes, conditionnement, prix
        });
        res.status(201).json({ message: 'Produit créé avec succès!', id: docRef.id });
      } catch (error) {
        res.status(500).json({ error: error.message });
      }
    });
  } else {
    res.status(405).json({ error: 'Method Not Allowed' });
  }
}