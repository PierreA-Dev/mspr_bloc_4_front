import { getDocs, collection, addDoc, doc, setDoc } from 'firebase/firestore';
import { db } from '../../../../firebase/initFirebase';

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader && authHeader.startsWith('Bearer ')) {
    const token = authHeader.split(' ')[1];
    // Vous pouvez ajouter ici la logique de vérification du token
    next();
  } else {
    res.status(401).json({ error: 'Unauthorized' });
  }
};

export default async function handler(req, res) {
  if (req.method === 'GET') {
    authenticateToken(req, res, async () => {
      try {
        const querySnapshot = await getDocs(collection(db, 'commandes'));
        const documents = [];
        for (const doc of querySnapshot.docs) {
          const numCommandesSnapshot = await getDocs(collection(doc.ref, 'numcommandes'));
          for (const numCommande of numCommandesSnapshot.docs) {
            const refProduitsSnapshot = await getDocs(collection(numCommande.ref, 'refproduits'));
            for (const refProduit of refProduitsSnapshot.docs) {
              documents.push({
                email: doc.id,
                numcommandes: numCommande.id,
                refproduits: refProduit.id,
                ...refProduit.data()
              });
            }
          }
        }
        res.status(200).json(documents);
      } catch (error) {
        res.status(500).json({ error: error.message });
      }
    });
  } else if (req.method === 'POST') {
    authenticateToken(req, res, async () => {
      try {
        const { email, numcommandes, refproduits, libelle, quantite, conditionnement, etat } = req.body;
        if (!email || !numcommandes || !refproduits || !libelle || !quantite || !conditionnement || !etat) {
          return res.status(400).json({ error: 'Tous les champs sont obligatoires.' });
        }

        const commandesRef = doc(db, 'commandes', email);
        const numCommandesRef = doc(commandesRef, 'numcommandes', numcommandes);
        const refProduitsRef = doc(numCommandesRef, 'refproduits', refproduits);

        await setDoc(numCommandesRef, { numcommandes });
        await setDoc(refProduitsRef, { libelle, quantite, conditionnement, etat });
        res.status(201).json({ message: 'Commande créée avec succès!' });
      } catch (error) {
        res.status(500).json({ error: error.message });
      }
    });
  } else {
    res.status(405).json({ error: 'Method Not Allowed' });
  }
}