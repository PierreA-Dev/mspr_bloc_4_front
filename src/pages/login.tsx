import React from 'react';
import AuthForm from '../components/AuthForm'; // Importer le composant AuthForm

const LoginPage: React.FC = () => {
  return (
    <AuthForm isSignup={false} />
  );
};

export default LoginPage;