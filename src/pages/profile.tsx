import React from 'react';
import { useAuth } from '../context/AuthContext';

const ProfilePage: React.FC = () => {
  const { user } = useAuth();

  if (!user) {
    return <div className="container mx-auto p-4">Veuillez vous connecter pour voir cette page.</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Mon Profil</h1>
      <p>Email : {user.email}</p>
    </div>
  );
};

export default ProfilePage;