import React, { createContext, useContext, useReducer, ReactNode } from 'react';

interface Product {
  id: string;
  reference: string;
  libelle: string;
  format: string;
  pays: string;
  intensite: string;
  aromes: string;
  conditionnement: string;
  image: string;
  prix: string;
}

interface CartState {
  products: Product[];
}

const initialState: CartState = {
  products: [],
};

type Action =
  | { type: 'ADD_TO_CART'; product: Product }
  | { type: 'CLEAR_CART' };

// Exporter CartContext pour qu'il puisse être utilisé ailleurs
export const CartContext = createContext<{ state: CartState; dispatch: React.Dispatch<Action> } | undefined>(undefined);

const cartReducer = (state: CartState, action: Action): CartState => {
  switch (action.type) {
    case 'ADD_TO_CART':
      return { ...state, products: [...state.products, action.product] };
    case 'CLEAR_CART':
      return { ...state, products: [] };
    default:
      throw new Error(`Unhandled action type: ${(action as Action).type}`);
  }
};

export const CartProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(cartReducer, initialState);
  const value = { state, dispatch };
  return <CartContext.Provider value={value}>{children}</CartContext.Provider>;
};

// Exporter useCart pour pouvoir l'utiliser ailleurs
export const useCart = () => {
  const context = useContext(CartContext);
  if (context === undefined) {
    throw new Error('useCart must be used within a CartProvider');
  }
  return context;
};