import React, { createContext, useContext, useState, useEffect, ReactNode } from 'react';
import { auth } from '../../firebase/initFirebase';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import { getFirestore, doc, getDoc } from 'firebase/firestore';

export const AuthContext = createContext<any>(null);

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [user, setUser] = useState<any>(null);
  const [token, setToken] = useState<string | null>(null);
  const [hasPermissions, setHasPermissions] = useState<boolean>(false);  // Vérifier seulement si le document existe
  const [loading, setLoading] = useState<boolean>(true);
  const [permissions, setPermissions] = useState<string[]>([]); // Initialise comme un tableau vide


  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, async (user) => {
      if (user) {
        setUser(user);
        const token = await user.getIdToken();
        setToken(token);

        // Vérifier si le document de l'utilisateur existe dans la collection 'permissions'
        try {
          const db = getFirestore();
          const docRef = doc(db, 'permissions', user.uid);
          const docSnap = await getDoc(docRef);

          if (docSnap.exists()) {
            setHasPermissions(true);  // L'utilisateur a des permissions
          } else {
            setHasPermissions(false); // Aucune permission trouvée
          }
        } catch (error) {
          console.error('Erreur lors de la récupération des permissions :', error);
          setHasPermissions(false); // Erreur dans la récupération des permissions
        }
      } else {
        setUser(null);
        setToken(null);
        setHasPermissions(false);  // L'utilisateur se déconnecte, pas de permissions
      }

      setLoading(false);  // Une fois la récupération terminée, on désactive le chargement
    });

    return () => unsubscribe();
  }, []);

  const logout = async () => {
    await signOut(auth);
    setUser(null);
    setToken(null);
    setHasPermissions(false);  // Réinitialiser les permissions lors de la déconnexion
  };

  return (
    <AuthContext.Provider value={{ user, token, hasPermissions, logout, loading }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);