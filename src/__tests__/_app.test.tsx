import React from 'react';
import { render } from '@testing-library/react';
import MyApp from '../pages/_app';
import { AppProps } from 'next/app';


jest.mock('../styles/globals.css', () => {});

// Mock des dépendances Firebase (si elles ne sont pas déjà mockées)
jest.mock('../../firebase/initFirebase', () => ({
  performance: jest.fn(),
}));

jest.mock('../components/Navbar', () => {
  const Navbar = () => <div data-testid="navbar">Navbar</div>;
  Navbar.displayName = 'Navbar';
  return Navbar;
});


jest.mock('../context/AuthContext', () => {
  const AuthProvider = ({ children }: { children: React.ReactNode }) => (
    <div data-testid="auth-provider">{children}</div>
  );
  AuthProvider.displayName = 'AuthProvider';
  return { AuthProvider };
});

jest.mock('../context/CartContext', () => {
  const CartProvider = ({ children }: { children: React.ReactNode }) => (
    <div data-testid="cart-provider">{children}</div>
  );
  CartProvider.displayName = 'CartProvider';
  return { CartProvider };
});


describe('MyApp', () => {
  const MockComponent = () => <div data-testid="mock-component">Page Content</div>;

  const mockPageProps: AppProps = {
    Component: MockComponent,
    pageProps: {},
  };

  test('renders AuthProvider, CartProvider, and Navbar', () => {
    const { getByTestId } = render(<MyApp {...mockPageProps} />);

    // Vérifie que les fournisseurs et la barre de navigation sont bien rendus
    expect(getByTestId('auth-provider')).toBeInTheDocument();
    expect(getByTestId('cart-provider')).toBeInTheDocument();
    expect(getByTestId('navbar')).toBeInTheDocument();
  });

  test('renders the passed page component', () => {
    const { getByTestId } = render(<MyApp {...mockPageProps} />);

    // Vérifie que le composant de la page est bien rendu
    expect(getByTestId('mock-component')).toBeInTheDocument();
    expect(getByTestId('mock-component').textContent).toBe('Page Content');
  });

  test('initializes Firebase performance only in the browser', () => {
    const initFirebase = require('../../firebase/initFirebase');
    render(<MyApp {...mockPageProps} />);
  });
});