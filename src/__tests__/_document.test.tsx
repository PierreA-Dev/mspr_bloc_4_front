import { render } from '@testing-library/react';
import Document from '../pages/_document'; // Assure-toi que le chemin est correct

// Mock des modules `next/document`
jest.mock('next/document', () => {
  return {
    Html: ({ children }: { children: React.ReactNode }) => <html>{children}</html>,
    Head: () => <head></head>,
    Main: () => <main></main>,
    NextScript: () => <script></script>,
  };
});

describe('Document Component', () => {
  it('devrait rendre correctement le document HTML de base', () => {
    const { container } = render(<Document />);

    // Vérifie que le document HTML contient les balises <html>, <head>, <main> et <script>
    expect(container.querySelector('html')).toBeInTheDocument();
    expect(container.querySelector('head')).toBeInTheDocument();
    expect(container.querySelector('main')).toBeInTheDocument();
    expect(container.querySelector('script')).toBeInTheDocument();
  });

  it('devrait avoir la langue anglaise définie', () => {
    const { container } = render(<Document />);
  });

  it('devrait contenir les éléments <Main> et <NextScript>', () => {
    const { container } = render(<Document />);

    // Vérifie que <Main> et <NextScript> sont présents dans le rendu
    expect(container.querySelector('main')).toBeInTheDocument();
    expect(container.querySelector('script')).toBeInTheDocument();
  });
});