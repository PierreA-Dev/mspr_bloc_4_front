import { render, screen } from '@testing-library/react';
import Home from '../pages'; // Assure-toi que le chemin est correct
import { useRouter } from 'next/router';

// Mock du hook useRouter
jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

describe('Home Page', () => {
  it('devrait rediriger vers /products après le rendu', () => {
    // Créer une fonction mockée pour useRouter
    const mockPush = jest.fn();
    (useRouter as jest.Mock).mockReturnValue({
      replace: mockPush, // Nous remplaçons replace par la fonction mockée
    });

    // Rendre le composant
    render(<Home />);

    // Vérifier que la fonction replace a été appelée avec /products
    expect(mockPush).toHaveBeenCalledWith('/products');
  });
});