import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import axios from 'axios';
import ManageProducts from '../pages/manage-products';
import { AuthContext } from '../context/AuthContext';

// Mock axios pour les appels API
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const mockUser = {
  getIdToken: jest.fn(() => Promise.resolve('mock-token')),
};

// Mock du useRouter de Next.js
jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

const mockProducts = [
  { id: '1', reference: 'REF1', libelle: 'Produit 1', format: 'Format 1', pays: 'France', intensite: '5', aromes: 'Arome 1', conditionnement: 'Boite', prix: '10' },
  { id: '2', reference: 'REF2', libelle: 'Produit 2', format: 'Format 2', pays: 'Espagne', intensite: '3', aromes: 'Arome 2', conditionnement: 'Sac', prix: '20' },
];

describe('ManageProducts Component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('devrait afficher la liste des produits', async () => {
    mockedAxios.get.mockResolvedValueOnce({ data: mockProducts });

    render(
      <AuthContext.Provider value={{ user: mockUser }}>
        <ManageProducts />
      </AuthContext.Provider>
    );

    // Vérifie que les produits sont affichés
    await waitFor(() => {
      expect(screen.getByText('Produit 1 - 10€')).toBeInTheDocument();
      expect(screen.getByText('Produit 2 - 20€')).toBeInTheDocument();
    });
  });

  it('devrait permettre la création d\'un produit', async () => {
    mockedAxios.get.mockResolvedValueOnce({ data: [] }); // Aucune donnée initiale
    mockedAxios.post.mockResolvedValueOnce({}); // Réponse pour la création

    render(
      <AuthContext.Provider value={{ user: mockUser }}>
        <ManageProducts />
      </AuthContext.Provider>
    );

    // Remplit le formulaire
    fireEvent.change(screen.getByPlaceholderText('Référence'), { target: { value: 'REF3' } });
    fireEvent.change(screen.getByPlaceholderText('Libellé'), { target: { value: 'Produit 3' } });
    fireEvent.change(screen.getByPlaceholderText('Format'), { target: { value: 'Format 3' } });
    fireEvent.change(screen.getByPlaceholderText('Pays'), { target: { value: 'Allemagne' } });
    fireEvent.change(screen.getByPlaceholderText('Intensité'), { target: { value: '4' } });
    fireEvent.change(screen.getByPlaceholderText('Arômes'), { target: { value: 'Arome 3' } });
    fireEvent.change(screen.getByPlaceholderText('Conditionnement'), { target: { value: 'Bouteille' } });
    fireEvent.change(screen.getByPlaceholderText('Prix'), { target: { value: '30' } });

    fireEvent.click(screen.getByText('Créer'));

    // Vérifie l'appel à l'API
    await waitFor(() => {
      expect(mockedAxios.post).toHaveBeenCalledWith(
        'https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits',
        {
          reference: 'REF3',
          libelle: 'Produit 3',
          format: 'Format 3',
          pays: 'Allemagne',
          intensite: '4',
          aromes: 'Arome 3',
          conditionnement: 'Bouteille',
          prix: '30',
        },
        { headers: { Authorization: 'Bearer mock-token' } }
      );
    });
  });
  
  it('devrait permettre la suppression d\'un produit', async () => {
    mockedAxios.get.mockResolvedValueOnce({ data: mockProducts });
    mockedAxios.delete.mockResolvedValueOnce({});

    render(
      <AuthContext.Provider value={{ user: mockUser }}>
        <ManageProducts />
      </AuthContext.Provider>
    );

    // Attends que les produits soient chargés
    await waitFor(() => {
      expect(screen.getByText('Produit 1 - 10€')).toBeInTheDocument();
    });

    fireEvent.click(screen.getAllByText('Supprimer')[0]); // Clique sur Supprimer pour le premier produit

    // Vérifie l'appel à l'API
    await waitFor(() => {
      expect(mockedAxios.delete).toHaveBeenCalledWith(
        'https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits/1',
        { headers: { Authorization: 'Bearer mock-token' } }
      );
    });
  });
});