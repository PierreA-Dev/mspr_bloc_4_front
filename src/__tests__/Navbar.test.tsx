import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { useRouter } from 'next/router';
import { useAuth } from '../context/AuthContext';
import Navbar from '../components/Navbar';

jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

jest.mock('../context/AuthContext', () => ({
  useAuth: jest.fn(),
}));

describe('Navbar', () => {
  const mockPush = jest.fn();
  const mockLogout = jest.fn();

  beforeEach(() => {
    (useRouter as jest.Mock).mockReturnValue({ asPath: '/products', push: mockPush });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('renders navigation links when user is not logged in', () => {
    (useAuth as jest.Mock).mockReturnValue({ user: null, logout: mockLogout });

    render(<Navbar />);

    expect(screen.getByText('PayeTonKawa')).toBeInTheDocument();
    expect(screen.getByText('Produits')).toBeInTheDocument();
    expect(screen.getByText('Panier')).toBeInTheDocument();
    expect(screen.getByText('Connexion')).toBeInTheDocument();
    expect(screen.queryByText('Déconnexion')).not.toBeInTheDocument();
  });

  test('renders user-specific links and logout button when user is logged in', () => {
    (useAuth as jest.Mock).mockReturnValue({ user: { name: 'Test User' }, logout: mockLogout });

    render(<Navbar />);

    expect(screen.getByText('PayeTonKawa')).toBeInTheDocument();
    expect(screen.getByText('Produits')).toBeInTheDocument();
    expect(screen.getByText('Panier')).toBeInTheDocument();
    expect(screen.getByText('Commandes')).toBeInTheDocument();
    expect(screen.getByText('Mon Profil')).toBeInTheDocument();
    expect(screen.getByText('Déconnexion')).toBeInTheDocument();
    expect(screen.queryByText('Connexion')).not.toBeInTheDocument();
  });

  test('redirects to login page with returnUrl when "Connexion" is clicked', () => {
    (useAuth as jest.Mock).mockReturnValue({ user: null, logout: mockLogout });

    render(<Navbar />);

    fireEvent.click(screen.getByText('Connexion'));
  });

  test('logs out and redirects to home page when "Déconnexion" is clicked', async () => {
    (useAuth as jest.Mock).mockReturnValue({ user: { name: 'Test User' }, logout: mockLogout });

    render(<Navbar />);

    fireEvent.click(screen.getByText('Déconnexion'));

    expect(mockLogout).toHaveBeenCalledTimes(1);
  });
});