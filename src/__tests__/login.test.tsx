import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import LoginPage from '../pages/login'; // Ajustez le chemin selon votre projet
import { useRouter } from 'next/router';
import React from 'react';


// Mock de `next/router`
jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

// Mock complet de Firebase
jest.mock('firebase/auth', () => ({
  getAuth: jest.fn(() => ({})),
  signInWithEmailAndPassword: jest.fn(),
}));

import { signInWithEmailAndPassword } from 'firebase/auth';

describe('LoginPage avec Firebase mocké', () => {
  const pushMock = jest.fn();

  beforeEach(() => {
    (useRouter as jest.Mock).mockReturnValue({ push: pushMock, query: {} });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('affiche un message d\'erreur pour des identifiants incorrects', async () => {
    (signInWithEmailAndPassword as jest.Mock).mockRejectedValue(new Error('Identifiants invalides'));
    render(<LoginPage />);

    const emailInput = screen.getByLabelText(/email/i);
    const passwordInput = screen.getByLabelText(/mot de passe/i);
    const loginButton = screen.getByRole('button', { name: /se connecter/i });

    fireEvent.change(emailInput, { target: { value: 'test@example.com' } });
    fireEvent.change(passwordInput, { target: { value: 'wrongpassword' } });
    fireEvent.click(loginButton);

    // Vérifiez que le message d'erreur s'affiche correctement
    const errorMessage = await screen.findByText(/Identifiants invalides/i);
    expect(errorMessage).toBeTruthy();
  });

  it('redirige vers la page des produits après une connexion réussie', async () => {
    // Mock de la fonction Firebase
    const signInWithEmailAndPasswordMock = jest.fn().mockResolvedValue({
      user: { getIdToken: jest.fn().mockResolvedValue('fake-token') },
    });
    jest.mocked(signInWithEmailAndPassword).mockImplementation(signInWithEmailAndPasswordMock);
  
    // Rendu du composant
    render(<LoginPage />);
  
    // Simulation des entrées utilisateur
    fireEvent.change(screen.getByLabelText(/Email/i), { target: { value: 'test@example.com' } });
    fireEvent.change(screen.getByLabelText(/Mot de passe/i), { target: { value: 'password123' } });
    fireEvent.click(screen.getByText(/Se connecter/i));
  
    // Attente de la fin de la logique asynchrone
    await waitFor(() => expect(signInWithEmailAndPasswordMock).toHaveBeenCalled());
  
    // Vérifie que `push` a été appelé avec la redirection correcte
    expect(pushMock).toHaveBeenCalledWith({
      pathname: '/products',
      query: { token: 'fake-token' },
    });
  });  

  it('redirige vers une URL personnalisée si "returnUrl" est spécifié', async () => {
    const signInWithEmailAndPasswordMock = jest.fn().mockResolvedValue({
      user: { getIdToken: jest.fn().mockResolvedValue('fake-token') },
    });
    jest.mocked(signInWithEmailAndPassword).mockImplementation(signInWithEmailAndPasswordMock);
  
    render(<LoginPage />);
  
    fireEvent.change(screen.getByLabelText(/Email/i), { target: { value: 'test@example.com' } });
    fireEvent.change(screen.getByLabelText(/Mot de passe/i), { target: { value: 'password123' } });
    fireEvent.click(screen.getByText(/Se connecter/i));
  
    await waitFor(() => expect(signInWithEmailAndPasswordMock).toHaveBeenCalled());
  
    // Vérifie que `push` a été appelé avec la redirection correcte
    expect(pushMock).toHaveBeenCalledWith({
      pathname: '/products',
      query: { token: 'fake-token' },
    });
  });  

  it('affiche un message d\'erreur si Firebase renvoie une exception générique', async () => {
    (signInWithEmailAndPassword as jest.Mock).mockRejectedValue(new Error('Erreur inconnue'));
    render(<LoginPage />);

    const emailInput = screen.getByLabelText(/email/i);
    const passwordInput = screen.getByLabelText(/mot de passe/i);
    const loginButton = screen.getByRole('button', { name: /se connecter/i });

    fireEvent.change(emailInput, { target: { value: 'test@example.com' } });
    fireEvent.change(passwordInput, { target: { value: 'correctpassword' } });
    fireEvent.click(loginButton);

    // Vérifiez que le message d'erreur s'affiche correctement
    const errorMessage = await screen.findByText(/Erreur inconnue/i);
    expect(errorMessage).toBeTruthy();
  });
});