import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import CartPage from '../pages/cart';
import { CartContext } from '../context/CartContext';
import { AuthContext } from '../context/AuthContext';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

// Mock next/router
jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

const mockRouter = {
  push: jest.fn(),
  replace: jest.fn(),
  prefetch: jest.fn(),
  back: jest.fn(),
  query: {},
  pathname: '/',
  asPath: '/',
};

// Mock Axios
const axiosMock = new MockAdapter(axios);

// Mock CartContext and AuthContext
const mockDispatch = jest.fn();
const mockCartState = {
  products: [{ libelle: 'Produit 1', prix: 100, conditionnement: 'Carton', email: 'useremail@gmail.com', numcommandes: '500100',
    refproduits: 'P001',
    quantite: 1,
    etat: 'En cours', }],
};
const mockUser = {
  email: 'user@example.com',
  getIdToken: jest.fn().mockResolvedValue('fake-token'),
};

describe('CartPage', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    const { useRouter } = require('next/router');
    useRouter.mockReturnValue(mockRouter); // Simuler useRouter ici
  });

  it('affiche un message lorsque le panier est vide', () => {
    render(
      <CartContext.Provider value={{ state: { products: [] }, dispatch: mockDispatch }}>
        <AuthContext.Provider value={{ user: mockUser }}>
          <CartPage />
        </AuthContext.Provider>
      </CartContext.Provider>
    );

    const emptyCartMessage = screen.queryByText(/Votre panier est vide/i);
    expect(emptyCartMessage).not.toBeNull();
  });

  it('redirige vers la page de connexion si non authentifié', () => {
    render(
      <CartContext.Provider value={{ state: mockCartState, dispatch: mockDispatch }}>
        <AuthContext.Provider value={{ user: null }}>
          <CartPage />
        </AuthContext.Provider>
      </CartContext.Provider>
    );

    fireEvent.click(screen.getByText(/Commander/i));
    expect(mockRouter.push).toHaveBeenCalledWith('/login');
  });

  it('crée une commande et vide le panier', async () => {
    axiosMock.onPost('https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes').reply(200);

    render(
      <CartContext.Provider value={{ state: mockCartState, dispatch: mockDispatch }}>
        <AuthContext.Provider value={{ user: mockUser }}>
          <CartPage />
        </AuthContext.Provider>
      </CartContext.Provider>
    );

    fireEvent.click(screen.getByText(/Commander/i));

    await waitFor(() => {
      expect(axiosMock.history.post.length).toBe(1);
      expect(mockDispatch).toHaveBeenCalledWith({ type: 'CLEAR_CART' });
      expect(mockRouter.push).toHaveBeenCalledWith('/orders');
    });
  });

it('affiche un bouton désactivé si aucun utilisateur n\'est connecté', () => {
  render(
    <CartContext.Provider value={{ state: mockCartState, dispatch: mockDispatch }}>
      <AuthContext.Provider value={{ user: null }}>
        <CartPage />
      </AuthContext.Provider>
    </CartContext.Provider>
  );

  const orderButton = screen.getByText(/Commander/i);
  expect(orderButton).toBeInTheDocument();
  expect(orderButton).toBeEnabled(); // Redirige vers login
});

it('affiche une erreur si un produit du panier est invalide', async () => {
  const invalidCartState = {
    products: [
      { libelle: '', prix: null, conditionnement: 'Carton', email: '', reference: '', quantite: 1 }
    ],
  };

  render(
    <CartContext.Provider value={{ state: invalidCartState, dispatch: mockDispatch }}>
      <AuthContext.Provider value={{ user: mockUser }}>
        <CartPage />
      </AuthContext.Provider>
    </CartContext.Provider>
  );

});

it('affiche une erreur si la commande échoue', async () => {
  axiosMock
    .onPost('https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes')
    .reply(500); // Simule une erreur serveur

  render(
    <CartContext.Provider value={{ state: mockCartState, dispatch: mockDispatch }}>
      <AuthContext.Provider value={{ user: mockUser }}>
        <CartPage />
      </AuthContext.Provider>
    </CartContext.Provider>
  );

  fireEvent.click(screen.getByText(/Commander/i));

    expect(mockDispatch).not.toHaveBeenCalled();
    expect(mockRouter.push).not.toHaveBeenCalled();
});


});