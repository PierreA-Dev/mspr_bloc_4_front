import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { CartProvider, useCart } from '../context/CartContext';

describe('CartContext', () => {
  const TestComponent = () => {
    const { state, dispatch } = useCart();

    return (
      <div>
        <div data-testid="cart-products">{JSON.stringify(state.products)}</div>
        <button
          data-testid="add-to-cart"
          onClick={() =>
            dispatch({
              type: 'ADD_TO_CART',
              product: {
                id: '1',
                reference: 'ref-001',
                libelle: 'Produit test',
                format: '500g',
                pays: 'France',
                intensite: 'Moyenne',
                aromes: 'Noisette',
                conditionnement: 'Paquet',
                image: 'image-url',
                prix: '5.00',
              },
            })
          }
        >
          Ajouter au panier
        </button>
        <button data-testid="clear-cart" onClick={() => dispatch({ type: 'CLEAR_CART' })}>
          Vider le panier
        </button>
      </div>
    );
  };

  test('should initialize with an empty cart', () => {
    const { getByTestId } = render(
      <CartProvider>
        <TestComponent />
      </CartProvider>
    );

    const cartProducts = getByTestId('cart-products');
    expect(cartProducts.textContent).toBe('[]'); // Le panier doit être vide au départ
  });

  test('should add a product to the cart', () => {
    const { getByTestId } = render(
      <CartProvider>
        <TestComponent />
      </CartProvider>
    );

    const addToCartButton = getByTestId('add-to-cart');
    fireEvent.click(addToCartButton);

    const cartProducts = getByTestId('cart-products');
    expect(cartProducts.textContent).toContain('Produit test');
    expect(cartProducts.textContent).toContain('5.00'); // Vérifier que le prix du produit est présent
  });

  test('should clear the cart', () => {
    const { getByTestId } = render(
      <CartProvider>
        <TestComponent />
      </CartProvider>
    );

    const addToCartButton = getByTestId('add-to-cart');
    const clearCartButton = getByTestId('clear-cart');

    // Ajouter un produit
    fireEvent.click(addToCartButton);

    const cartProductsBeforeClear = getByTestId('cart-products');
    expect(cartProductsBeforeClear.textContent).toContain('Produit test');

    // Vider le panier
    fireEvent.click(clearCartButton);

    const cartProductsAfterClear = getByTestId('cart-products');
    expect(cartProductsAfterClear.textContent).toBe('[]');
  });
});