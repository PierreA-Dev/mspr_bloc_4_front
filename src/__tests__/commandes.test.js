import request from 'supertest';
import { createMocks } from 'node-mocks-http';
import handler from '../pages/api/commandes/commandes'; // Le chemin de ton fichier API
import '@testing-library/jest-dom';

// Helper pour créer des mocks
const createMockRequest = (method, body = {}, headers = {}) => {
  const { req, res } = createMocks({
    method,
    body,
    headers,
  });
  return { req, res };
};

// Test de la requête GET
describe('GET /commandes', () => {
  it('devrait récupérer les commandes et produits associés', async () => {
    const { req, res } = createMockRequest('GET', {}, {
      Authorization: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjBhYmQzYTQzMTc4YzE0MjlkNWE0NDBiYWUzNzM1NDRjMDlmNGUzODciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbXNwci1ibG9jLTQtNTEzNWEiLCJhdWQiOiJtc3ByLWJsb2MtNC01MTM1YSIsImF1dGhfdGltZSI6MTczNzQwODc4MCwidXNlcl9pZCI6ImRMTW11bzdKYVlQeW1FTlUwc3FkYlJ2c0poWDIiLCJzdWIiOiJkTE1tdW83SmFZUHltRU5VMHNxZGJSdnNKaFgyIiwiaWF0IjoxNzM3NDA4NzgwLCJleHAiOjE3Mzc0MTIzODAsImVtYWlsIjoiZmFubnlAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbImZhbm55QGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.IBKLhqO4DJaRle7GxNolGgZAs707fm8xld46Um8369LYh8ae1Fj0n8FuHw4tJ3fO85B7nv0R1tfJO-slyXfDKURnUc1mJsIDrxISq8-oJ09j4dQ1sMLAMNEekmGxRj4lwesnixrNfLlWNSn07e0-3nQOndnpMySCV1gbvWUEsJthjUi_SBLKq8xZRDslIs0O1h6QL2H68seHLmfmb7XzK9II78nUKHPOknPG4Slm4_nUf-0DhRfp2DLNJ_P46XcWyzmnQMvc7VnBxZ_pvI1wmv458hJWMwPKWwSqvL7Pui7ibA3n3uvaPT_YptmY_Jk8VdfxBSeLypx2lWPxf2XnIw',
    });

    await handler(req, res);
  });

  it('devrait renvoyer une erreur 401 si le token est invalide', async () => {
    const { req, res } = createMockRequest('GET', {}, {
      Authorization: 'FTTCKYJ1515535',
    });

    await handler(req, res);

    expect(res.statusCode).toBe(401); // Vérifie que le code de statut est 401
    expect("{\"error\":\"Unauthorized\"}");
  });
});

  it('devrait renvoyer une erreur 401 si des champs sont manquants', async () => {
    const { req, res } = createMockRequest('POST', {
      email: 'test@example.com',
      numcommandes: '12345',
      refproduits: 'product-1',
      libelle: 'Produit Test',
      quantite: 10,
      // conditionnement et etat manquants
    }, {
      Authorization: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjBhYmQzYTQzMTc4YzE0MjlkNWE0NDBiYWUzNzM1NDRjMDlmNGUzODciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbXNwci1ibG9jLTQtNTEzNWEiLCJhdWQiOiJtc3ByLWJsb2MtNC01MTM1YSIsImF1dGhfdGltZSI6MTczNzQwODc4MCwidXNlcl9pZCI6ImRMTW11bzdKYVlQeW1FTlUwc3FkYlJ2c0poWDIiLCJzdWIiOiJkTE1tdW83SmFZUHltRU5VMHNxZGJSdnNKaFgyIiwiaWF0IjoxNzM3NDA4NzgwLCJleHAiOjE3Mzc0MTIzODAsImVtYWlsIjoiZmFubnlAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbImZhbm55QGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.IBKLhqO4DJaRle7GxNolGgZAs707fm8xld46Um8369LYh8ae1Fj0n8FuHw4tJ3fO85B7nv0R1tfJO-slyXfDKURnUc1mJsIDrxISq8-oJ09j4dQ1sMLAMNEekmGxRj4lwesnixrNfLlWNSn07e0-3nQOndnpMySCV1gbvWUEsJthjUi_SBLKq8xZRDslIs0O1h6QL2H68seHLmfmb7XzK9II78nUKHPOknPG4Slm4_nUf-0DhRfp2DLNJ_P46XcWyzmnQMvc7VnBxZ_pvI1wmv458hJWMwPKWwSqvL7Pui7ibA3n3uvaPT_YptmY_Jk8VdfxBSeLypx2lWPxf2XnIw',
    });

    await handler(req, res);

    expect(res.statusCode).toBe(401); // Vérifie que le code de statut est 400
    expect("{\"error\":\"Unauthorized\"}");
  });

  it('devrait renvoyer une erreur 401 si le token est invalide', async () => {
    const { req, res } = createMockRequest('POST', {
      email: 'test@example.com',
      numcommandes: '12345',
      refproduits: 'product-1',
      libelle: 'Produit Test',
      quantite: 10,
      conditionnement: 'Boîte',
      etat: 'En attente',
    }, {
      Authorization: 'FHIFKYUGVYOOBXU65215785534CFRNUYBHGTSVIL',
    });

    await handler(req, res);

    expect(res.statusCode).toBe(401); // Vérifie que le code de statut est 401
    expect("{\"error\":\"Unauthorized\"}");
});