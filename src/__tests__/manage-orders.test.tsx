import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import axios from 'axios';
import ManageOrdersPage from '../pages/manage-orders'; // Adaptez le chemin si nécessaire
import { useAuth } from '../context/AuthContext';
import React from 'react';

// Mock de useAuth pour simuler un utilisateur authentifié
jest.mock('../context/AuthContext', () => ({
  useAuth: jest.fn(),
}));

// Mock d'axios
jest.mock('axios');

describe('ManageOrdersPage', () => {
  const mockUser = {
    getIdToken: jest.fn().mockResolvedValue('fake-token'),
  };

  beforeEach(() => {
    (useAuth as jest.Mock).mockReturnValue({ user: mockUser });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('affiche un message lorsque aucune commande n\'est disponible', async () => {
    (axios.get as jest.Mock).mockResolvedValue({ data: [] });

    render(<ManageOrdersPage />);
  });

  it('charge et affiche une liste de commandes', async () => {
    const mockOrders = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        conditionnement: 'boîte',
        etat: 'en cours',
      },
      {
        email: 'test2@example.com',
        numcommandes: '12346',
        refproduits: 'A002',
        libelle: 'Produit 2',
        quantite: 1,
        conditionnement: 'paquet',
        etat: 'livré',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockOrders });

    render(<ManageOrdersPage />);

    await waitFor(() => {
      expect(screen.getByText(/Produit 1/)).toBeInTheDocument();
      expect(screen.getByText(/Produit 2/)).toBeInTheDocument();
    });
  });

  it('gère une erreur lors de la récupération des commandes', async () => {
    (axios.get as jest.Mock).mockRejectedValue(new Error('Erreur API'));

    render(<ManageOrdersPage />);

  });

  it('met à jour une commande avec succès', async () => {
    const mockOrders = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        conditionnement: 'boîte',
        etat: 'en cours',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockOrders });

    render(<ManageOrdersPage />);

    await waitFor(() => screen.getByText(/Produit 1/));

    fireEvent.click(screen.getByText(/Modifier/));
    fireEvent.change(screen.getByDisplayValue(/Produit 1/), { target: { value: 'Produit modifié' } });

    (axios.put as jest.Mock).mockResolvedValue({});

    fireEvent.click(screen.getByText(/Sauvegarder/));

    await waitFor(() => {
      expect(axios.put).toHaveBeenCalledWith(
        'https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes/test@example.com/12345/A001',
        expect.objectContaining({ libelle: 'Produit modifié' }),
        { headers: { Authorization: 'Bearer fake-token' } }
      );
    });
  });

  it('affiche un message d\'erreur en cas d\'échec de la mise à jour', async () => {
    const mockOrders = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        conditionnement: 'boîte',
        etat: 'en cours',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockOrders });

    render(<ManageOrdersPage />);

    await waitFor(() => screen.getByText(/Produit 1/));

    fireEvent.click(screen.getByText(/Modifier/));
    fireEvent.change(screen.getByDisplayValue(/Produit 1/), { target: { value: 'Produit modifié' } });

    (axios.put as jest.Mock).mockRejectedValue(new Error('Erreur mise à jour'));

    fireEvent.click(screen.getByText(/Sauvegarder/));
  });

  it('supprime une commande avec succès', async () => {
    const mockOrders = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        conditionnement: 'boîte',
        etat: 'en cours',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockOrders });

    render(<ManageOrdersPage />);

    await waitFor(() => screen.getByText(/Produit 1/));

    (axios.delete as jest.Mock).mockResolvedValue({});

    fireEvent.click(screen.getByText(/Supprimer/));

    await waitFor(() => {
      expect(axios.delete).toHaveBeenCalledWith(
        'https://mspr-bloc-4-back-commandes-405778108809.europe-west9.run.app/commandes/test@example.com/12345/A001',
        { headers: { Authorization: 'Bearer fake-token' } }
      );
    });
  });

  it('affiche un message d\'erreur en cas d\'échec de la suppression', async () => {
    const mockOrders = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        conditionnement: 'boîte',
        etat: 'en cours',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockOrders });

    render(<ManageOrdersPage />);

    await waitFor(() => screen.getByText(/Produit 1/));

    (axios.delete as jest.Mock).mockRejectedValue(new Error('Erreur suppression'));

    fireEvent.click(screen.getByText(/Supprimer/));
  });

  it('annule la modification d\'une commande', async () => {
    const mockOrders = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        conditionnement: 'boîte',
        etat: 'en cours',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockOrders });

    render(<ManageOrdersPage />);

    await waitFor(() => screen.getByText(/Produit 1/));

    fireEvent.click(screen.getByText(/Modifier/));
    fireEvent.change(screen.getByDisplayValue(/Produit 1/), { target: { value: 'Produit modifié' } });
    fireEvent.click(screen.getByText(/Annuler/));

    expect(screen.queryByDisplayValue(/Produit modifié/)).not.toBeInTheDocument();
  });
});