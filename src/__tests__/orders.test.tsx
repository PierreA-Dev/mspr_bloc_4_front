import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import axios from 'axios';
import OrdersPage from '../pages/orders' // Adaptez le chemin si nécessaire
import { useAuth } from '../context/AuthContext';
import { useRouter } from 'next/router';
import React from 'react';
import '@testing-library/jest-dom';


// Mock du hook useAuth pour simuler un utilisateur authentifié
jest.mock('../context/AuthContext', () => ({
  useAuth: jest.fn(),
}));

// Mock de useRouter pour simuler la navigation
jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

// Mock d'axios
jest.mock('axios');

describe('OrdersPage', () => {
  const mockUser = {
    email: 'test@example.com',
    getIdToken: jest.fn().mockResolvedValue('fake-token'),
  };

  beforeEach(() => {
    // On simule un utilisateur authentifié
    (useAuth as jest.Mock).mockReturnValue({ user: mockUser });

    // Mock de la méthode push du router (pour tester la redirection)
    (useRouter as jest.Mock).mockReturnValue({
      push: jest.fn(),
    });
  });

  it('devrait afficher les commandes récupérées depuis l\'API', async () => {
    // On simule la réponse d'axios pour l'appel GET
    const mockCommandes = [
      {
        email: 'test@example.com',
        numcommandes: '12345',
        refproduits: 'A001',
        libelle: 'Produit 1',
        quantite: 2,
        poids: 5,
        date_expedition: '2025-01-01',
        conditionnement: 'boîte',
        etat: 'en cours',
      },
    ];

    (axios.get as jest.Mock).mockResolvedValue({ data: mockCommandes });

    render(<OrdersPage />);

    // Attendre que les commandes soient récupérées et affichées
    await waitFor(() => {
      expect(screen.getByText(/Produit 1/)).toBeInTheDocument();
      expect(screen.getByText(/2025-01-01/)).toBeInTheDocument();
    });
  });

  it('devrait afficher un message de chargement au début', () => {
    render(<OrdersPage />);
    expect(screen.getByText(/Chargement.../)).toBeInTheDocument();
  });

  it('devrait rediriger vers la page de connexion si l\'utilisateur n\'est pas connecté', async () => {
    // Mock de l'utilisateur non authentifié
    (useAuth as jest.Mock).mockReturnValue({ user: null });

    render(<OrdersPage />);
  });

  it('devrait afficher un message si aucune commande n\'est trouvée', async () => {
    // On simule une réponse vide de l'API
    (axios.get as jest.Mock).mockResolvedValue({ data: [] });

    render(<OrdersPage />);
  });

  it('devrait afficher une erreur si l\'appel API échoue', async () => {
    // Simuler une erreur lors de la récupération des commandes
    (axios.get as jest.Mock).mockRejectedValue(new Error('Erreur API'));

    render(<OrdersPage />);
  });
});