import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import SignupPage from '../pages/signup'
import { useRouter } from 'next/router';
import '@testing-library/jest-dom';
import React from 'react';

// On mocke la fonction `useRouter` de Next.js
jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

// Mock complet de Firebase
jest.mock('firebase/auth', () => ({
  getAuth: jest.fn(() => ({})),
  createUserWithEmailAndPassword: jest.fn(),
}));

import { createUserWithEmailAndPassword } from 'firebase/auth';

jest.mock('firebase/performance', () => ({
  getPerformance: jest.fn(),
}));


describe('SignupPage', () => {
  const mockRouterPush = jest.fn();
  const mockCreateUserWithEmailAndPassword = createUserWithEmailAndPassword as jest.Mock;

  beforeEach(() => {
    (useRouter as jest.Mock).mockReturnValue({
      push: mockRouterPush,
    });
    mockCreateUserWithEmailAndPassword.mockClear();
    mockRouterPush.mockClear();
  });

  it('devrait inscrire un utilisateur et rediriger vers la page de connexion', async () => {
    // On simule un retour réussi de la fonction `createUserWithEmailAndPassword`
    mockCreateUserWithEmailAndPassword.mockResolvedValueOnce({});

    render(<SignupPage />);

    // Remplir le formulaire
    fireEvent.change(screen.getByLabelText(/Email/), { target: { value: 'test@example.com' } });
    fireEvent.change(screen.getByLabelText(/Mot de passe/), { target: { value: 'password123' } });
    fireEvent.change(screen.getByLabelText(/Confirmer le mot de passe/), { target: { value: 'password123' } });

    // Soumettre le formulaire
    fireEvent.click(screen.getByText(/S'inscrire/));

    // Vérifier que la fonction Firebase a été appelée
    await waitFor(() => {
      expect(mockCreateUserWithEmailAndPassword).toHaveBeenCalledWith(
        expect.any(Object), // auth (firebase)
        'test@example.com',
        'password123'
      );
    });

    // Vérifier que l'utilisateur a été redirigé
    expect(mockRouterPush).toHaveBeenCalledWith('/login');
  });

  it('devrait afficher une erreur si les mots de passe ne correspondent pas', () => {
    render(<SignupPage />);

    // Remplir le formulaire avec des mots de passe différents
    fireEvent.change(screen.getByLabelText(/Email/), { target: { value: 'test@example.com' } });
    fireEvent.change(screen.getByLabelText(/Mot de passe/), { target: { value: 'password123' } });
    fireEvent.change(screen.getByLabelText(/Confirmer le mot de passe/), { target: { value: 'password456' } });

    // Soumettre le formulaire
    fireEvent.click(screen.getByText(/S'inscrire/));

    // Vérifier que le message d'erreur est affiché
    expect(screen.getByText(/Les mots de passe ne correspondent pas/)).toBeInTheDocument();
  });

  it('devrait afficher une erreur si une erreur se produit lors de l\'inscription', async () => {
    // On simule une erreur de Firebase
    mockCreateUserWithEmailAndPassword.mockRejectedValueOnce(new Error('Email déjà utilisé'));

    render(<SignupPage />);

    // Remplir le formulaire avec un email et un mot de passe
    fireEvent.change(screen.getByLabelText(/Email/), { target: { value: 'test@example.com' } });
    fireEvent.change(screen.getByLabelText(/Mot de passe/), { target: { value: 'password123' } });
    fireEvent.change(screen.getByLabelText(/Confirmer le mot de passe/), { target: { value: 'password123' } });

    // Soumettre le formulaire
    fireEvent.click(screen.getByText(/S'inscrire/));

    // Attendre que l'erreur soit affichée
    await waitFor(() => {
      expect(screen.getByText('Email déjà utilisé')).toBeInTheDocument();
    });
  });
});