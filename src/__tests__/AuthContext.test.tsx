import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { AuthProvider, useAuth } from '../context/AuthContext';
import '@testing-library/jest-dom';

// Configuration de l'adaptateur axios-mock
const mock = new MockAdapter(axios);

// Composant de test
const TestComponent = () => {
  const { user, token, permissions, logout, loading } = useAuth();
  if (loading) return <div>Loading...</div>;
  return (
    <div>
      <span>{user ? `User: ${user.email}` : 'No user'}</span>
      <span>{token ? `Token: ${token}` : 'No token'}</span>

      <button onClick={logout}>Logout</button>
    </div>
  );
};

describe('AuthContext with Axios Mock', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    mock.reset(); // Réinitialisation de l'adaptateur avant chaque test
  });

  it('should render loading initially', async () => {
    // Simulation d'un appel API avec Axios pour récupérer l'utilisateur
    mock.onGet('/user').reply(200, {
      email: 'test@example.com',
      token: 'fake-token',
    });

    // Effectuer des tests avec AuthProvider
    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
  });

  it('should handle user login and permissions', async () => {
    // Simulation d'un appel API pour récupérer les permissions
    mock.onGet('/user').reply(200, { email: 'test@example.com', token: 'fake-token' });
    mock.onGet('/permissions').reply(200, { access: ['manageProducts', 'manageOrders'] });

    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
  });

  it('should show "No user" and "No permissions" if no user is logged in', async () => {
    // Simulation d'un appel API pour aucun utilisateur connecté
    mock.onGet('/user').reply(200, null);

    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
  });

  it('should handle logout', async () => {
    // Simulation d'un appel API pour l'utilisateur connecté
    mock.onGet('/user').reply(200, { email: 'test@example.com', token: 'fake-token' });

    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
  });
});