import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import ProductsPage from '../pages/products';  // Adapte ce chemin selon ton projet
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { CartProvider } from '../context/CartContext';  // Si tu utilises un contexte pour le panier

// Créer une instance du mock pour simuler les appels HTTP
const mockAxios = new MockAdapter(axios);

// Exemple de produit pour le test
const mockProducts = [
  {
    id: '1',
    reference: 'ref-001',
    libelle: 'Produit 1',
    format: 'Format A',
    pays: 'France',
    intensite: 'Moyenne',
    aromes: 'Fruits',
    conditionnement: 'Bouteille',
    image: 'product1.jpg',
    prix: '10',
  },
  {
    id: '2',
    reference: 'ref-002',
    libelle: 'Produit 2',
    format: 'Format B',
    pays: 'Espagne',
    intensite: 'Élevée',
    aromes: 'Épices',
    conditionnement: 'Boîte',
    image: 'product2.jpg',
    prix: '20',
  },
];

describe('ProductsPage', () => {
  beforeEach(() => {
    // Réinitialiser les mocks avant chaque test
    mockAxios.reset();
  });

  test('affiche un message de chargement pendant que les produits sont récupérés', () => {
    // Simuler une requête qui prend du temps
    mockAxios.onGet('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits').reply(200, []);

    render(
      <CartProvider>
        <ProductsPage />
      </CartProvider>
    );

    expect(screen.getByText(/chargement/i)).toBeInTheDocument();
  });

  test('affiche les produits après le chargement', async () => {
    // Simuler la réponse API avec des produits
    mockAxios.onGet('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits').reply(200, mockProducts);

    render(
      <CartProvider>
        <ProductsPage />
      </CartProvider>
    );

    // Vérifier que le message de chargement disparait
    await waitFor(() => expect(screen.queryByText(/chargement/i)).not.toBeInTheDocument());

    // Vérifier que les produits sont rendus
    expect(screen.getByText(/Produit 1/i)).toBeInTheDocument();
    expect(screen.getByText(/Produit 2/i)).toBeInTheDocument();
    expect(screen.getByText(/Prix: 10 €/i)).toBeInTheDocument();
    expect(screen.getByText(/Pays: France/i)).toBeInTheDocument();
  });

  test('ajoute un produit au panier', async () => {
    // Simuler la réponse API avec des produits
    mockAxios.onGet('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits').reply(200, mockProducts);

    const { container } = render(
      <CartProvider>
        <ProductsPage />
      </CartProvider>
    );

    // Attendre que les produits se chargent
    await waitFor(() => expect(screen.queryByText(/chargement/i)).not.toBeInTheDocument());

    // Simuler un clic sur le bouton "Ajouter au panier" pour le premier produit
    const addToCartButton = container.querySelector('button');
    fireEvent.click(addToCartButton!);

    // Vérifier que la fonction dispatch est appelée (tu peux aussi utiliser des mocks pour spy sur dispatch)
    expect(screen.getByText(/Produit 1/i)).toBeInTheDocument();  // Vérifie que le produit est toujours affiché
  });

  test('affiche un message d\'erreur en cas d\'échec de la récupération des produits', async () => {
    // Simuler une erreur lors de la récupération des produits
    mockAxios.onGet('https://mspr-bloc-4-back-produits-405778108809.europe-west9.run.app/produits').reply(500);

    render(
      <CartProvider>
        <ProductsPage />
      </CartProvider>
    );

    // Vérifier que le message de chargement disparait et qu'aucun produit n'est affiché
    await waitFor(() => expect(screen.queryByText(/chargement/i)).not.toBeInTheDocument());
  });
});