import { render, screen } from '@testing-library/react';
import ProfilePage from '../pages/profile';
import { useAuth } from '../context/AuthContext';
import '@testing-library/jest-dom';
import React from 'react';

// Mock de useAuth
jest.mock('../context/AuthContext', () => ({
  useAuth: jest.fn(),
}));

describe('ProfilePage', () => {
  it('devrait afficher le profil lorsque l\'utilisateur est connecté', () => {
    // Simulation d'un utilisateur authentifié
    (useAuth as jest.Mock).mockReturnValue({
      user: { email: 'test@example.com' },
    });

    render(<ProfilePage />);

    // Vérifier que l'email de l'utilisateur est affiché
    expect(screen.getByText(/Mon Profil/)).toBeInTheDocument();
    expect(screen.getByText(/Email : test@example.com/)).toBeInTheDocument();
  });

  it('devrait afficher un message de connexion lorsque l\'utilisateur n\'est pas connecté', () => {
    // Simulation de l'absence d'utilisateur
    (useAuth as jest.Mock).mockReturnValue({
      user: null,
    });

    render(<ProfilePage />);

    // Vérifier qu'un message de connexion est affiché
    expect(screen.getByText(/Veuillez vous connecter pour voir cette page./)).toBeInTheDocument();
  });
});