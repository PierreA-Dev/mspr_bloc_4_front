import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useAuth } from '../context/AuthContext';

const Navbar: React.FC = () => {
  const { user, logout, hasPermissions, loading } = useAuth();
  const router = useRouter();
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const handleLogout = async () => {
    await logout();
    router.push('/');
  };

  return (
    <nav className="bg-yellow-500 p-4 shadow-lg z-40">
      <div className="container mx-auto flex justify-between items-center">
        <div className="text-white text-2xl font-bold">
          <Link href="/">PayeTonKawa</Link>
        </div>

        {/* Button Burger (visible sur tous les écrans) */}
        <div className="block lg:block">
          <button
            onClick={() => setIsMenuOpen(!isMenuOpen)}
            className="text-white focus:outline-none"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          </button>
        </div>
      </div>

      {/* Menu déroulant sur tous les écrans */}
      <div
        role="button"
        tabIndex={0}
        className={`fixed top-0 left-0 w-full h-screen bg-black bg-opacity-50 z-40 transform ${
          isMenuOpen ? 'translate-x-0' : '-translate-x-full'
        } transition-all duration-300`}
        onClick={() => setIsMenuOpen(false)} // Ferme le menu si on clique à l'extérieur
        onKeyDown={(e) => {
          // Ferme le menu au clavier (Enter, Espace, etc.)
          if (e.key === 'Enter' || e.key === ' ') {
            setIsMenuOpen(false);
          }
        }}
      >
        <div
          className={`bg-yellow-500 w-3/4 lg:w-1/4 h-full overflow-y-auto p-6 transform ${
            isMenuOpen ? 'translate-x-0' : '-translate-x-full'
          } transition-all duration-300 z-50`}
        >
          {/* Liens du menu déroulant */}
          <Link
            href="/products"
            className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
          >
            Produits
          </Link>
          <Link
            href="/cart"
            className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
          >
            Panier
          </Link>

          {user ? (
            <>
              <Link
                href="/orders"
                className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
              >
                Commandes
              </Link>
              <Link
                href="/profile"
                className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
              >
                Mon Profil
              </Link>

              {!loading && hasPermissions && (
                <>
                  <Link
                    href="/manage-products"
                    className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
                  >
                    Gérer produits
                  </Link>
                  <Link
                    href="/manage-orders"
                    className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
                  >
                    Gérer commandes
                  </Link>
                </>
              )}

              <button
                onClick={handleLogout}
                className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
              >
                Déconnexion
              </button>
            </>
          ) : (
            <Link
              href={{ pathname: '/login', query: { returnUrl: router.asPath } }}
              className="text-white py-2 px-4 block hover:text-yellow-300 transition duration-300 text-sm sm:text-base md:text-lg"
            >
              Connexion
            </Link>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;