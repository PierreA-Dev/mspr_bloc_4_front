import React, { useState, FormEvent } from 'react';
import { useRouter } from 'next/router';
import { auth } from '../../firebase/initFirebase';
import { signInWithEmailAndPassword, createUserWithEmailAndPassword } from 'firebase/auth';

interface AuthFormProps {
  isSignup: boolean; // True si c'est la page d'inscription, false si c'est la page de connexion
}

const AuthForm: React.FC<AuthFormProps> = ({ isSignup }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [error, setError] = useState('');
  const router = useRouter();

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();
    if (isSignup && password !== confirmPassword) {
      setError('Les mots de passe ne correspondent pas');
      return;
    }

    try {
      if (isSignup) {
        // Inscription
        await createUserWithEmailAndPassword(auth, email, password);
        router.push('/login'); // Redirige vers la page de connexion après inscription
      } else {
        // Connexion
        const userCredential = await signInWithEmailAndPassword(auth, email, password);
        const idToken = await userCredential.user?.getIdToken();
        if (idToken) {
          // Redirige vers la page des produits avec le token
          const returnUrl = router.query.returnUrl || '/products';
          router.push({
            pathname: returnUrl as string,
            query: { token: idToken }
          });
        }
      }
    } catch (error) {
      console.error('Erreur:', error);
      if (error instanceof Error) {
        setError(error.message);
      }
    }
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">{isSignup ? 'Inscription' : 'Connexion'}</h1>
      {error && <p className="text-red-500">{error}</p>}
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <label htmlFor="email" className="block text-gray-700">Email</label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            className="input input-bordered w-full"
            required
          />
        </div>
        <div className="mb-4">
          <label htmlFor="password" className="block text-gray-700">Mot de passe</label>
          <input
            id="password"
            type="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            className="input input-bordered w-full"
            required
          />
        </div>
        {isSignup && (
          <div className="mb-4">
            <label htmlFor="confirmPassword" className="block text-gray-700">Confirmer le mot de passe</label>
            <input
              id="confirmPassword"
              type="password"
              value={confirmPassword}
              onChange={e => setConfirmPassword(e.target.value)}
              className="input input-bordered w-full"
              required
            />
          </div>
        )}
        <button type="submit" className="btn btn-primary">{isSignup ? 'S\'inscrire' : 'Se connecter'}</button>
      </form>
      {!isSignup && (
        <p className="mt-4">
          <button className="btn btn-secondary" onClick={() => router.push('/signup')}>Inscription</button>
        </p>
      )}
    </div>
  );
};

export default AuthForm;