export const generateId = (): string => {
  return Math.random().toString().slice(2, 7); // Génère une chaîne de 5 chiffres
};