module.exports = {
  testEnvironment: 'jest-environment-jsdom', // Définir une seule fois l'environnement jsdom
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest', // Utiliser babel-jest pour transformer les fichiers .ts/.tsx/.js/.jsx
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'], // Extensions reconnues par Jest
  setupFilesAfterEnv: ['./jest.setup.js'], // Définir les fichiers de configuration après l'environnement de test
  testMatch: [
    "**/tests/**/*.[jt]s?(x)", // Cherche tous les tests dans le dossier __tests__
    "**/?(*.)+(spec|test).[tj]s?(x)", // Recherche des fichiers .spec et .test
  ],
  collectCoverage: true, // Activer la collecte de couverture
  collectCoverageFrom: [
    'src/**/*.{js,ts,tsx}', // Collecter la couverture de tous les fichiers dans `src/`
    '!src/**/*.d.ts', // Exclure les fichiers de déclaration TypeScript
    '!src/**/*.config.js', // Exemple : Exclure certains fichiers comme les fichiers de config
  ],
  coverageDirectory: './coverage', // Dossier où les rapports de couverture seront générés
  coverageReporters: ["lcov", "text"]
};